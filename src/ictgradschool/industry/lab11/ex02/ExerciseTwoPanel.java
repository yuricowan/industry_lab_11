package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{

    /**
     * Creates a new ExerciseFivePanel.
     */
    private JButton addButton;
    private JButton subtractButton;

    private JTextField inputNumber1;
    private JTextField inputNumber2;
    private JTextField result;


    public ExerciseTwoPanel() {
        setBackground(Color.white);

        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");

        inputNumber1 = new JTextField(15);
        inputNumber2 = new JTextField(15);
        result = new JTextField(20);

        this.add(inputNumber1);
        this.add(inputNumber2);
        this.add(addButton);
        this.add(subtractButton);
        this.add(result);


        addButton.addActionListener(this);
        subtractButton.addActionListener(this);

    }
    public void actionPerformed(ActionEvent event) {

        double doubleInputNumber1 = Double.parseDouble(inputNumber1.getText());
        double doubleInputNumber2 = Double.parseDouble(inputNumber2.getText());

        if (event.getSource() == addButton) {
            double addedNumber = doubleInputNumber1 + doubleInputNumber2;
            double rounded = roundTo2DecimalPlaces(addedNumber);
            result.setText(rounded + "");
        }
        else if (event.getSource() == subtractButton) {
            double subtractedNumber = doubleInputNumber1 - doubleInputNumber2;
            double rounded = roundTo2DecimalPlaces(subtractedNumber);
            result.setText(rounded + "");
        }


    }
    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}