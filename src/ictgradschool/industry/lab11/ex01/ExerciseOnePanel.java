package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField height;
    private JTextField weight;
    private JTextField yourBMI;
    private JTextField maxHealthyWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeightButton = new JButton("Calculate Healthy Weight");

        height = new JTextField(15);
        weight = new JTextField(15);
        yourBMI = new JTextField(15);
        maxHealthyWeight = new JTextField(15);


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightText = new JLabel("Height in meters: ");
        JLabel weightText = new JLabel("Weight in kilograms: ");
        JLabel yourBMIText = new JLabel("Your Body Mass Index (BMI) is: ");
        JLabel healthyWeightText = new JLabel("Maximum Healthy Weight for your Height: ");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightText);
        this.add(height);
        this.add(weightText);
        this.add(weight);
        this.add(calculateBMIButton);
        this.add(yourBMIText);
        this.add(yourBMI);
        this.add(calculateHealthyWeightButton);
        this.add(healthyWeightText);
        this.add(maxHealthyWeight);



        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeightButton.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        double doubleHeight = Double.parseDouble(height.getText());
        double doubleWeight = Double.parseDouble(weight.getText());


        if (event.getSource() == calculateBMIButton) {
           double unroundedAnswer = doubleWeight / (doubleHeight * doubleHeight);
           double roundedAnswer = roundTo2DecimalPlaces(unroundedAnswer);
           yourBMI.setText(roundedAnswer + "");
       }
       else if (event.getSource() == calculateHealthyWeightButton) {
            double unroundedAnswer = 24.9 * doubleHeight * doubleHeight;
            double roundedAnswer = roundTo2DecimalPlaces(unroundedAnswer);
            maxHealthyWeight.setText(roundedAnswer + "");

       }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}