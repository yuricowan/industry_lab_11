package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Balloon balloon;
    private JButton moveButton;
    private Timer timer;
    private ArrayList<Balloon>balloonsList = new ArrayList<Balloon>();

    /**
     * Creates a new ExerciseFourPanel.
     */

    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);
//
//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);
        this.addKeyListener(this);
        this.timer = new Timer(1, this);




    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        balloon.move();
        for (int i = 0; i < balloonsList.size(); i++) {
            balloonsList.get(i).move();
        }
        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        requestFocusInWindow();

            balloon.draw(g);

        for (int i = 0; i < balloonsList.size(); i++) {
            balloonsList.get(i).draw(g);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_UP) {
            balloon.setDirection(Direction.Up);
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            timer.start();
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            balloon.setDirection(Direction.Down);
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) Math.random() * 1000));
            timer.start();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            balloon.setDirection(Direction.Left);
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            timer.start();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            balloon.setDirection(Direction.Right);
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            timer.start();
        }
        else if(e.getKeyCode() == KeyEvent.VK_S) {
            timer.stop();
        }
        else if (e.getKeyCode() == KeyEvent.VK_F){
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));
            balloonsList.add(new Balloon((int) (Math.random() * 1000), (int) (Math.random() * 1000)));




        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}